#!/bin/sh
# -*- scheme -*-
exec guile -e main -s "$0" "$@"
!#

;;; This file is part of Flutemetal.
;;;
;;; Flutemetal is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Magic is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Flutemetal.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (ice-9 getopt-long))

(define (overdrive)
  (display "Overdrive effect ...")
  (newline))

(define (equalizer)
  (display "Equalizer effect ...")
  (newline))

(define (chorus)
  (display "Chorus effect ...")
  (newline))

(define (delay)
  (display "Delay effect ...")
  (newline))

(define (version)
  (display "\
Flutemetal 0.0.1
Copyright (C) 2016 Daniel Pimentel

License GPLv3+: GNU GPL 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.")
  (newline))

(define (help)
  (display "\
Usage: flutemetal [OPTION]... [CODE]...
Evaluate code with Flutemetal, interactively or from a script.

  -o     overdrive effect on or off
  -e     equalizer effect on or off
  -c     chorus effect on or off
  -d     delay effect on or off
  -v     display this help and exit
  -h     display version information and exit

Report bugs to: d4n1@d4n1.org
Flutemetal home page: <http://www.gitlab.com/d4n1/flutemetal>")
  (newline))

(define (main args)
  (let* ((option-spec '((overdrive-option (single-char #\o) (value #f))
			(equalizer-option (single-char #\e) (value #f))
			(chorus-option (single-char #\c) (value #f))
			(delay-option (single-char #\d) (value #f))
			(version-option (single-char #\v) (value #f))
			(help-option (single-char #\h) (value #f))))
	 (options (getopt-long args option-spec))
	 (overdrive-option (option-ref options 'overdrive-option #f))
	 (equalizer-option (option-ref options 'equalizer-option #f))
	 (chorus-option (option-ref options 'chorus-option #f))
	 (delay-option (option-ref options 'delay-option #f))
	 (version-option (option-ref options 'version-option #f))
	 (help-option (option-ref options 'help-option #f)))
    (if (or overdrive-option equalizer-option chorus-option
	    delay-option version-option help-option)
	(begin
	  (if overdrive-option (overdrive))
	  (if equalizer-option (equalizer))
	  (if chorus-option (chorus))
	  (if delay-option (delay))
	  (if version-option (version))
	  (if help-option (help)))
	(begin
	  (version)
	  (newline)))))
