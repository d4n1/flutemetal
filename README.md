# Flutemetal

Flutemetal is a synthesizer multi-effects to flute developed in GNU Guile.

## Requeriments
* guile
* lilypond

## Usage
### Effects
```
flutemetal -d -c 
```

### Version
```
flutemetal -v
```

### Help
```
flutemetal -h
```
